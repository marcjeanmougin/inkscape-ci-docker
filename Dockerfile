FROM ubuntu:23.10
ENV DEBIAN_FRONTEND=noninteractive
ARG CI
ADD install_dependencies.sh /
RUN /install_dependencies.sh --full 
RUN apt install -y libgstreamer-plugins-bad1.0-dev mm-common meson
RUN git clone https://gitlab.gnome.org/GNOME/gtkmm.git && cd gtkmm && meson build && cd build && ninja && ninja install
